# mHBS - Mobile Helping Babies Breathe
The mobile Helping Babies Survive (mHBS) app is a digital health tool, developed by collaborators at Indiana University (School of Medicine and School of Informatics and Computing), which consists of 3 separate apps, serving functions related to data collection and reporting, access to educational resources, including other apps and data visualization. The mHBS apps are powered by the District Health Information Software ([DHIS 2](https://dhis2.org)) in the back-end for data storage and tracking of health workers.

# mHBS Training App
The mHBS Training App is a community health worker training app focused on training for neonatal resuscitation, through videos, guide and other kinds of training material. As part of the [mHBS suite of apps](https://www.iu.edu/~neoinfo/services/), after community health workers have been trained, then can test and their follow-up scores are tracked through the [mHBS tracker](https://github.com/iupui-soic/dhis2-android-trackercapture), a fork of the DHIS2 tracker app. 

## Tech Stack
* Framework 7
* Cordova

## Usage
### 1. Clone the Repository
```
git clone https://github.com/iupui-soic/mHBS_tracker
```
### 2. Add OS specific platforms

For Android:
Go to the downloaded repository folder and run:
```
cordova platform add android
```

### 3. Build the app
(plugins will be automatically built)
```
cordova build
```

### 4. Run or emulate the app
To run the app on android (... assuming that an Android device is connected)
```
cordova run android
```

OR to emulate on the Android emulator (... assuming that the Android SDK/Android Emulator has been installed)
```
cordova emulate android
```

## Features
### 1. Offline support and sync facility for Media Page
 - List of documents on the media page is in synchronisation with the resources uploaded on the dhis2 and also available in offline mode.
 - For every document when the user clicks, if it is already downloaded it will be played using the fileopener2 Cordova plugin.
 - If the user is connected to the internet then whenever he clicks on the document it will get synced with the dhis2 and a new copy will be
   saved to File System local device, Corresponding file location will be updated to SQLite-DB as well.
 - A separate icon is also placed on every document to show its sync status.
 - On ongoing sync, a progress bar is visible on the document.

 * For better understanding of all logic and implementation details please check [this](https://bhavesh3005sharma-gsoc21.blogspot.com/2021/07/fourth-week-of-coding-26-june-2july.html).


 ### 2. App Usage tracking system
 - Usage of the app can be tracked over dhis2.
 - App Usage ( Page Visits and Time Spent for each page ) will be stored locally in SQL DB.
 - When usage crosses a threshold limit then this usage will be updated to the user's data store at dhis2 by API calls.
 - And the local SQL DB reset to store new usages data.
 - To store app usage for every page, a random and fixed id is given which works as a key for a page in the user's datastore.
 - Usages are stored in the "trainer-app-usage" namespace with a random and fixed key for everypage.
 - Schema of JSON stored by keys in data store :
{
"page_name": "homepage",
"page_visits": 4,
"page_time_spent": 2.5443666666666664
}

 
